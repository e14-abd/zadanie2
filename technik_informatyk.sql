/*Przykładowa baza danych dla strony WWW wspomagającej uczenie się zagadnień do egzaminów z technik informatyk. 
Plik zawiera przykładowe tabele, które muszą bezwzględnie znaleźć się w projekcie. Tabele mogą być natomiast ŻLE zaprojektowanie.
Należy przepjektowanie tabele tak by były bardziej efektywne oraz UJEDNOLICONE (typy podobhnych pól powinny być takie same).   

Baza powinna pozwalać na przechowywanie:
- danych o nauczycielach
- testy praktyczne, tj. pozwalające na dodawanie odpowiedzi w postaci multimedialnej (przykładowo kodu HTML/CSS/JS, odpowiedzi z pytań przeciągnij i upuść itd.)
- testy teoretycznej (pytanie + 4 odpowiedzi, wskazanie na poprawną odpowiedź)
- informacje o postępie nauki (uwzględniając nowe oraz stare egzaminy)

Ponadto trzeba odpowiedzieć na pytanie dlaczego ostatnie polecenie po uruchomieniu powoduje błąd. Czy można w jakiś sposób ten błąd pominąć/wyeliminować?
*/
DROP DATABASE IF EXISTS `technik_informatyk`;
CREATE DATABASE `technik_informatyk`DEFAULT CHARACTER SET UTF8 DEFAULT COLLATE utf8_general_ci;
USE `technik_informatyk`;
CREATE TABLE `uzytkownicy` (`id_uzytkownik` SERIAL, `imie` CHAR(30), `nazwisko` CHAR(40), `ulica` CHAR(40), `miasto` CHAR(60), `nr_domu` INT(5), `nick` VARCHAR(5), `haslo` CHAR(67) COMMENT 'haslo zapisane w SHA256', `poczta` VARCHAR(100) COMMENT 'Poczta elektorniczna') ENGINE=INNODB;  
CREATE TABLE `artykuly` (`id_artykul` SERIAL, `nazwa` CHAR(30), `tresc` LONGTEXT, `data_dodania` CHAR(10), `id_uzytkownik` BIGINT(20) UNSIGNED, `id_kategoria` INT(11) UNSIGNED NOT NULL, `id_dzial` VARCHAR(100) NOT NULL) ENGINE InnoDB; 

CREATE TABLE `ketegorie` (`id_kategoria` INT(11) NOT NULL AUTO_INCREMENT, `nazwa` CHAR(30), `komentarz` TEXT, `id_uzytkownik` BIGINT(20) UNSIGNED COMMENT 'Opiekun kategorii', PRIMARY KEY(`id_kategoria`)) ENGINE INNODB;
CREATE TABLE `galerie` (`id_galeria` SERIAL, `nazwa` CHAR(30), `opis` VARCHAR(200), `id_uzytkownik` BIGINT(20) UNSIGNED COMMENT 'Opiekun galerii', `data_dodania` DATE) COMMENT 'Tabela może zawierać zarówno zdjęcia jak i kolekcje plików do pobrania' ENGINE InnoDB;

CREATE TABLE `pliki` (`id_plik` SERIAL, `nazwa` CHAR(30), `typ` INT(2) UNSIGNED, `id_uzytkownik` BIGINT(20) UNSIGNED COMMENT 'Użytkownik dodający', `data_dodania` DATE, `dane` BLOB) ENGINE InnoDB;

CREATE TABLE `galeria_plik` (`id_plik` BIGINT(20) UNSIGNED, `id_galeria` BIGINT(20) UNSIGNED) ENGINE=InnoDB;

CREATE INDEX `index_gp_plik` ON `galeria_plik`(`id_plik`) USING HASH;
CREATE INDEX `index_gp_galeria` ON `galeria_plik`(`id_galeria`) USING HASH;

ALTER TABLE `galeria_plik` ADD CONSTRAINT `fk_gp_plik` FOREIGN KEY (`id_plik`) REFERENCES `pliki`(`id_plik`) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE `galeria_plik` ADD CONSTRAINT `fk_gp_galeria` FOREIGN KEY (`id_galeria`) REFERENCES `galerie`(`id_galeria`) ON UPDATE CASCADE ON DELETE CASCADE;

INSERT INTO `galerie` (`nazwa`,`opis`) VALUES ('Nowa galeria', 'Pierwsza przykładowa galeria'), ('Pliki do pobrania', 'Galeria z plkami do pobrania');
INSERT INTO `pliki`(`nazwa`, `typ`) VALUES ('Obrazek na stronę', 2), ('Arkusz z 2015', 1), ('Logo firmy', 2), ('Wygląd strony WWW', 2), ('Kryteria oceniania', 1);                                               
                                               
INSERT INTO `galeria_plik` VALUES (1,1),(3,1), (4,1), (2,2), (5,2);
/*INSERT INTO `galeria_plik` VALUES (6,3);*/